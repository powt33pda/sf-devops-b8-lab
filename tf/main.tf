terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.68.0"
    }
  }
}

provider "yandex" {
  token     = "AQAAAAAeIycyAATuwcvgdQvyOEEdo2uGoApwbcE"
  cloud_id  = "cloud-33pda"
  folder_id = "b1gk2ai7nhqsar0e9k55"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "jenkins" {
  name        = "jenkins"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = "jenkins"

  resources {
    cores  = 4
    memory = 8
  }

  boot_disk {
    initialize_params {
      image_id = "fd80viupr3qjr5g6g9du"
      size = 50
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.sf-b8-5.id}"
    ipv4 = "true"
    nat = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("../keys/jenkins.pub")}"
    serial-port-enable: "1"
  }
}

resource "yandex_vpc_network" "sf-b8-5" { }
  
resource "yandex_vpc_subnet" "sf-b8-5" {
    zone       = "ru-central1-a"
    network_id = "${yandex_vpc_network.sf-b8-5.id}"
    v4_cidr_blocks = ["172.16.0.0/16"]
}
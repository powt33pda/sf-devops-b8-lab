URL="localhost:9889"
RESPONSE=$(curl -i $URL 2> /dev/null | head -n 1 | cut -d$' ' -f2)

[[ -z "$RESPONSE" ]] && { echo "Error: No answer from server."; exit 1; }
[ ! $RESPONSE -eq 200 ] && { echo "Error: Invalid response from server: $RESPONSE."; exit 1; }

SERVER_MD5="$(curl -sL $URL | md5sum | cut -d ' ' -f 1)"
FILE_MD5="$(md5sum "./html/index.html" | cut -d ' ' -f 1)"

[ ! "$SERVER_MD5" == "$FILE_MD5" ] && { echo "Error: HTML file MD5 mismatch. SERVER MD5: $SERVER_MD5, FILE MD5: $FILE_MD5."; exit 1; }

echo "MD5 of HTML from Nginx matched with MD5 of the local file."
exit 0